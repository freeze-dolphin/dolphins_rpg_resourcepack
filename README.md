# dolphins_rpg_resourcepack

A compound rpg resourcepack.

## ordering

1. **Minutiae v5.0**

2. **Jehkoba's Fantasy**
3. **Jehkoba's Item Names**
4. **Wynncraft**
5. **Scope**
6. **Coverer**

## containments

- Based on: **Jehkoba's Fantasy**
- Hotbar Icons: **GeruDoku**

- Fonts & Some Sounds: **Wynncraft**
- Dynamic Icons & Dynamic Items & Paintings & Some Blocks: **史莱姆的细节材质包** (QQ Group: 1127186770)

- Scope Styled Pumpkinblur: **Flans Mod ** - **ModernWarfare** (Modified by myself)
